# Movie App


Deployed at: 

https://react-movie-web-app-eta.vercel.app/


## screenshot
![](demo-movie.png)

### Tools used

* react v16 +
* react-router v4
* react-bootstrap
* react-router-bootstrap
* styled-components
* axios
* themoviedb-API

> Dont forget to change your api key * you can get API key from themoviedb.org *

